<?php

namespace Guestbook\BookBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Guestbook\BookBundle\Entity\Post;
use Guestbook\BookBundle\Form\Book\PostForm;


class BookController extends Controller
{

    public function indexAction()
    {

	      $all_posts = $this->getDoctrine()
		                 ->getRepository('GuestbookBookBundle:Post')
		                 ->findAllPosts($approved = false);

        return $this->render('GuestbookBookBundle:Book:index.html.twig',
			     array('posts' => $all_posts)
		    );
    }


    public function aboutAction()
    {
	      return $this->render('GuestbookBookBundle:Book:about.html.twig');
    }

    public function postCommentAction(Request $request) {

      // Get all requests values
        $tabPost = $request->request->all();
	      $new_post = new Post();

        $new_post->setName($tabPost['name']);
        $new_post->setEmail($tabPost['email']);
        $new_post->setComment($tabPost['message']);

	      $em = $this->getDoctrine()->getEntityManager();
	      $em->persist($new_post);
	      $em->flush();

	      return $this->redirect($this->generateUrl("GuestbookBookBundle_homepage"));
    }

} // class
